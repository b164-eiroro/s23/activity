// creating a single room using insert
db.rooms.insert({
        name : "Single",
        accomodate : "2",
        price : "1000",
        description : "A simple room with all the basic necessities",
        rooms_available : "10",
        isAvailable : false
    }
    )

// creating a two rooms using a insertMany
db.rooms.insertMany([
        {
            name : "double",
            accomodates : "3",
            price : "1000",
            description : "A room fit for a small family going on a vacation.",
            rooms_available : "5",
            isAvailable : false
         },
         {
            accomodates : "4",
            price : "4000",
            description : "A room with a queen  sized bed perfect for a simple getaway",
            rooms_available : "15",
            isAvailable : false, 
          }
    ]
    )

// finding a room that has a name of double
db.rooms.find({
        "name" : "double"
    }
    )

// updating queen room to availability of 0
db.rooms.updateOne(
        { accomodates : 4 },
        { $set : {
            rooms_available : 0,
             }
        }
     )


/*{
    "acknowledged" : true,
    "matchedCount" : 1.0,
    "modifiedCount" : 1.0
}*/

// deleting an onject that has room availability of 0
db.rooms.deleteMany({
        rooms_available : 0
    }
    )

/*{
    "acknowledged" : true,
    "deletedCount" : 1.0
}*/